module.exports = {
    devServer: {
        disableHostCheck: true,
        host: "frontend.dev.local",
        port: 80,
    },
};