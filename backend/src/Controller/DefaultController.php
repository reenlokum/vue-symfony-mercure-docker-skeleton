<?php

namespace App\Controller;

use App\MercureSubscriber\MercureSubscriberService;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;

class DefaultController
{
    /**
     * @var PublisherInterface
     */
    private $publisher;

    public function __construct(PublisherInterface $publisher)
    {
        $this->publisher = $publisher;
    }

    public function index()
    {
        $update = new Update('http://frontend.dev.local/test.json', 'This is a message');
        $this->publisher->__invoke($update);

        return new Response('OK!');
    }

    public function login(Request $request)
    {
        $now = new DateTimeImmutable();
        $expiresAt = $now->add(new \DateInterval('PT1M'));

        $service = new MercureSubscriberService($_ENV['MERCURE_JWT_KEY']);
        $token = $service->createToken(['http://frontend.dev.local/test.json'], $expiresAt);

        $cookie = new Cookie(
            'mercureAuthorization',
            $token,
            $expiresAt,
            '/',
            $request->getHost(),
            false,
            true
        );

        $response = new JsonResponse([
            'expiresAt' => $expiresAt->getTimestamp(),
        ], JsonResponse::HTTP_OK);
        $response->headers->setCookie($cookie);

        return $response;
    }
}
