<?php

namespace App\MercureSubscriber;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

class MercureSubscriberService
{
    /** @var string */
    private $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param string[]           $topics
     * @param \DateTimeImmutable $expiresAt
     *
     * @return string
     */
    public function createToken(array $topics, \DateTimeImmutable $expiresAt)
    {
        $builder = new Builder();

        return $builder
            ->set('mercure', [
                'subscribe' => $topics,
            ])
            ->setExpiration($expiresAt->getTimestamp())
            ->getToken(new Sha256(), new Key($this->key));
    }
}
